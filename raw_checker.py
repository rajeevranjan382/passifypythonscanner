from astroid import nodes

from pylint.checkers import BaseChecker
from pylint.interfaces import IRawChecker


class MyRawChecker(BaseChecker):
    """check for line continuations with '\' instead of using triple
    quoted string or parenthesis
    """

    __implements__ = IRawChecker

    name = "custom_raw"
    msgs = {
        "W9901": (
            "Don't use File I/O operation",
            "file-io-operation",
            (
                "Don't use File Input Output Operation"
            ),
        ),
        "W9902": (
            "Don't use logging operation",
            "logger",
            (
                "Don't use logging Operation"
            ),
        ),
        "W9903": (
            "Don't use DB connection operation",
            "db-connection",
            (
                "Don't use Db Connection Operation"
            ),
        ),
        "W9904": (
            "Don't use RabbitMQ",
            "rabbitmq-import",
            (
                "Don't use RabbitMQ import"
            ),
        ),
        "W9905": (
            "Don't use RabbitMQ operation",
            "rabbitmq-operation",
            (
                "Don't use RabbitMQ Operation"
            ),
        ),
        "W9906": (
            "Don't use kafka import",
            "kafka-import",
            (
                "Don't use kafka import"
            ),
        ),
        "W9907": (
            "Don't use kafka operation",
            "kafka-operation",
            (
                "Don't use kafka Operation"
            ),
        ),
        "W9908": (
            "Don't oauth operation",
            "oauth-operation",
            (
                "Don't use oauth Operation"
            ),
        ),
        "W9909": (
            "Don't use tomcat",
            "tomcat-import",
            (
                "Don't use tomcat"
            ),
        ),
        "W9910": (
            "Don't use tomcat-manager",
            "tomcat-manager",
            (
                "Don't use tomcat-manager"
            ),
        ),
        "W9911": (
            "Don't use jboss",
            "jboss-import",
            (
                "Don't use jboss"
            ),
        ),
        "W9912": (
            "Don't use mysql",
            "mysql-import",
            (
                "Don't use mysql"
            ),
        ),
        "W9913": (
            "Don't use mysql-instance",
            "mysql-instance",
            (
                "Don't use mysql-instance"
            ),
        ),
        "W9914": (
            "Don't use oracle-odbc",
            "oracle-odbc-import",
            (
                "Don't use oracle-odbc-import"
            ),
        ),
        "W9915": (
            "Don't use oracle-odbc",
            "oracle-odbc-instance",
            (
                "Don't use oracle-odbc-instance"
            ),
        ),
        "W9916": (
            "Don't use ibm_db",
            "ibm_db-import",
            (
                "Don't use ibm_db-import"
            ),
        ),
        "W9917": (
            "Don't use ibm_db-fetch_assoc",
            "fetch_assoc-instance",
            (
                "Don't use ibm_db-fetch_assoc-instance"
            ),
        ),
        "W9918": (
            "Don't use ibm_db_dbi",
            "ibm_db_dbi-import",
            (
                "Don't use ibm_db_dbi-import"
            ),
        ),
        "W9919": (
            "Don't use cx_Oracle",
            "cx_Oracle-import",
            (
                "Don't use cx_Oracle-import"
            ),
        ),
        "W9920": (
            "Don't use pymongo",
            "pymongo-import",
            (
                "Don't use pymongo-import"
            ),
        ),
        "W9921": (
            "Don't use MongoClient",
            "MongoClient-instance",
            (
                "Don't use MongoClient-instance"
            ),
        ),
        "W9922": (
            "Don't use redis",
            "redis-import",
            (
                "Don't use redis-import"
            ),
        ),
        "W9923": (
            "Don't use zeep",
            "zeep-import",
            (
                "Don't use SOAP-client-zeep"
            ),
        ),
        "W9925": (
            "Don't use requests.get",
            "requests.get-instance",
            (
                "Don't use HTTP-requests.get"
            ),
        ),
        "W9926": (
            "Don't use requests.put",
            "requests.put-instance",
            (
                "Don't use HTTP-requests.put"
            ),
        ),
        "W9927": (
            "Don't use requests.post",
            "requests.post-instance",
            (
                "Don't use HTTP-requests.post"
            ),
        ),
        "W9928": (
            "Don't use Vault-hvac",
            "hvac-import",
            (
                "Don't use hvac-import"
            ),
        ),
        "W9929": (
            "Don't use smtplib",
            "smtplib-import",
            (
                "Don't use smtplib-import"
            ),
        ),
        "W9931": (
            "Don't use subprocess",
            "subprocess-import",
            (
                "Don't use System-subprocess"
            ),
        ),
        "W9932": (
            "Don't use threading",
            "threading-import",
            (
                "Don't use threading-import"
            ),
        ),
        "W9933": (
            "Don't use os",
            "os-import",
            (
                "Don't use os-import"
            ),
        ),
        "W9934": (
            "Don't use ldap",
            "ldap-import",
            (
                "Don't use ldap-import"
            ),
        ),
        "W9935": (
            "Don't use easyad",
            "easyad-import",
            (
                "Don't use Authentication-easyad-import"
            ),
        ),
        "W9936": (
            "Don't use requests.auth",
            "requests.auth-import",
            (
                "Don't use Authentication-requests.auth-import"
            ),
        ),
        "W9937": (
            "Don't use HTTPBasicAuth",
            "HTTPBasicAuth-import",
            (
                "Don't use BasicAuthentication-HTTPBasicAuth-import"
            ),
        ),
        "W9938": (
            "Don't use Data compression",
            "data-compression-import",
            (
                "Don't use data-compression-import"
            ),
        ),
        "W9939": (
            "Don't use Data Encryption",
            "data-encryption-import",
            (
                "Don't use data-encryption-import"
            ),
        ),
        "W9940": (
            "Don't use socket",
            "socket-import",
            (
                "Don't use socket-import"
            ),
        ),
        "W9941": (
            "Don't use snmp",
            "pysnmp-import",
            (
                "Don't use pysnmp-import"
            ),
        ),
        "W9942": (
            "Don't use loadbalancer",
            "loadbalancer-import",
            (
                "Don't use loadbalancer-import"
            ),
        ),
        "W9943": (
            "Don't use udp-socket",
            "SOCK_DGRAM",
            (
                "Don't use udp-socket-connection"
            ),
        ),
        "W9944": (
            "Don't use tcp-socket",
            "SOCK_STREAM",
            (
                "Don't use tcp-socket-connection"
            ),
        )

    }

    options = ()

    def process_module(self, node: nodes.Module) -> None:
        """process a module
        the module's content is accessible via node.stream() function
        """
        with node.stream() as stream:
            import pdb

            for (lineno, line) in enumerate(stream):
                op_line_string = line.decode("utf-8")
                # pdb.set_trace()
                if "open" in op_line_string:
                    self.add_message('file-io-operation', line=lineno)
                elif "logger." in op_line_string:
                    self.add_message('logger', line=lineno)
                elif ".connect" in op_line_string:
                    self.add_message('db-connection', line=lineno)
                elif "import pika" in op_line_string:
                    self.add_message('rabbitmq-import', line=lineno)
                elif "pika.BlockingConnection" in op_line_string:
                    self.add_message('rabbitmq-operation', line=lineno)
                elif "redirect_uri" in op_line_string:
                    self.add_message('oauth-operation', line=lineno)
                elif "import KafkaConsumer" in op_line_string or "import KafkaProducer" in op_line_string:
                    self.add_message('kafka-import', line=lineno)
                elif "KafkaProducer(" in op_line_string or "KafkaConsumer(" in op_line_string:
                    self.add_message('kafka-operation', line=lineno)
                elif "import tomcat" in op_line_string or "import tomcatmanager" in op_line_string:
                    self.add_message('tomcat-import', line=lineno)
                elif "TomcatManager" in op_line_string:
                    self.add_message('tomcat-manager', line=lineno)
                elif "import PyJboss" in op_line_string:
                    self.add_message('jboss-import', line=lineno)
                elif "import mysql" in op_line_string:
                    self.add_message('mysql-import', line=lineno)
                elif "mysql.connector.connect" in op_line_string:
                    self.add_message('mysql-instance', line=lineno)
                elif "import pyodbc" in op_line_string:
                    self.add_message('oracle-odbc-import', line=lineno)
                elif "pyodbc.connect" in op_line_string:
                    self.add_message('oracle-odbc-instance', line=lineno)
                elif "from ibm_db" in op_line_string:
                    self.add_message('ibm_db-import', line=lineno)

                elif "fetch_assoc" in op_line_string:
                    self.add_message('fetch_assoc-instance', line=lineno)

                elif "import ibm_db_dbi" in op_line_string:
                    self.add_message('ibm_db_dbi-import', line=lineno)

                elif "import cx_Oracle" in op_line_string:
                    self.add_message('cx_Oracle-import', line=lineno)

                elif "import pymongo" in op_line_string:
                    self.add_message('pymongo-import', line=lineno)

                elif "MongoClient" in op_line_string:
                    self.add_message('MongoClient-instance', line=lineno)

                elif "import redis" in op_line_string or "redis.Redis" in op_line_string:
                    self.add_message('redis-import', line=lineno)

                elif "from zeep" in op_line_string:
                    self.add_message('zeep-import', line=lineno)

                elif "requests.get" in op_line_string:
                    self.add_message('requests.get-instance', line=lineno)

                elif "requests.put" in op_line_string:
                    self.add_message('requests.put-instance', line=lineno)

                elif "requests.post" in op_line_string:
                    self.add_message('requests.post-instance', line=lineno)

                elif "import hvac" in op_line_string:
                    self.add_message('hvac-import', line=lineno)
                elif "import smtplib" in op_line_string:
                    self.add_message('smtplib-import', line=lineno)

                elif "import subprocess" in op_line_string:
                    self.add_message('subprocess-import', line=lineno)

                elif "import os" in op_line_string:
                    self.add_message('os-import', line=lineno)

                elif "import smtplib" in op_line_string:
                    self.add_message('smtplib-import', line=lineno)

                elif "import ldap" in op_line_string or "ldap.initialize" in op_line_string:
                    self.add_message('ldap-import', line=lineno)

                elif "from easyad" in op_line_string or "import EasyAD" in op_line_string:
                    self.add_message('easyad-import', line=lineno)

                elif "from requests.auth" in op_line_string:
                    self.add_message('requests.auth-import', line=lineno)

                elif "import HTTPBasicAuth" in op_line_string:
                    self.add_message('HTTPBasicAuth-import', line=lineno)

                elif "import gzip" in op_line_string or "import zlib" in op_line_string:
                    self.add_message('data-compression-import', line=lineno)

                elif "import base64" in op_line_string or "import hashlib" in op_line_string or "import paperclib" in op_line_string:
                    self.add_message('data-encryption-import', line=lineno)

                elif "import socket" in op_line_string:
                    self.add_message('socket-import', line=lineno)

                elif "from pysnmp" in op_line_string or "import pysnmp" in op_line_string:
                    self.add_message('pysnmp-import', line=lineno)
                elif "from loadbalancer" in op_line_string or "import loadbalancer" in op_line_string:
                    self.add_message('loadbalancer-import', line=lineno)
                elif "SOCK_DGRAM" in op_line_string:
                    self.add_message('SOCK_DGRAM', line=lineno)
                elif "SOCK_STREAM" in op_line_string:
                    self.add_message('SOCK_STREAM', line=lineno)





def register(linter):
    """required method to auto register this checker"""
    linter.register_checker(MyRawChecker(linter))

