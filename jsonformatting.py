with open("pyoutjson.json", "r") as f:
    lines = f.readlines()

flag = 0
with open("pyoutjson.json", "w") as f:
    for line in lines:
        if flag ==0 and "[" in line:
            flag = 1
        if flag ==1:
            f.write(line)
import json

f = open('pyoutjson.json','r' )

data = json.load(f)
f.close()

for i in data:
    i['lineNumber'] = i.pop('line')
    i['linesOfCode'] = 0
    i['efforts'] = 0
    i['application'] = i.pop('module')
    i['migrationIndex'] = 0
    i['sourceType'] = "python"
    i['applicationId'] = 0
    i['category'] = i.pop('symbol')
    i['value'] = i.pop('message')
    i['fileType'] = "python"
    i.pop('type')
    i.pop('obj')
    i.pop('path')
    i.pop('column')
    i.pop('message-id')
a_file = open("pyoutjson.json", "w")
json.dump(data, a_file)
a_file.close()

#import pandas
#pandas.read_json("sysorule.json").to_excel("sysoruleoutput.xlsx")
"""
"linesOfCode": 724,
"efforts": 3.5,
"application": "java-postgresql-tutorial",
"migrationIndex": 3.5,
"sourceType": "java",
"lineNumber": 19,
"applicationId": "J-1",
"category": "Logging",
"value": "AvoidLogger",
"fileType": "java"

"""

