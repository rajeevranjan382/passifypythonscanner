import luddite
lines = []
with open('requirement.txt') as file:
    lines = file.readlines()
    lines = [line.rstrip() for line in lines]

dependencies_list = []
lines = ["boto3==1.0.2","Pillow==2.3.4"]
for module in lines:
    module_name = module.split("==")[0]
    current_version = module.split("==")[1]
    latest_version = luddite.get_version_pypi(module_name)
    info =  "Upgrade to latest version " + latest_version
    if(current_version == latest_version):
        info = "Up to date"
    dict = {"artifactId":module_name,"category":"other","version":current_version,"info":info, "latest_version":latest_version}
    dependencies_list.append(dict)

versions = [{"name": "QA Controls Web","dependencies":dependencies_list}]
import json
with open('versionfile.json', 'w') as fout:
    json.dump(versions , fout)
